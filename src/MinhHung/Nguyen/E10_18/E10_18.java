package MinhHung.Nguyen.E10_18;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
public class E10_18 {
    static int count = 0;
    public static void main(String[] args) {

        JPanel panel = new JPanel();
        final JButton button = new JButton("Click me!");
        panel.add(button);
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(200, 150);
        frame.add(panel);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);/* aligning window at the center */
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                count++;
                button.setText("I was clicked " + count + " times!");
            }
        });
    }
}
