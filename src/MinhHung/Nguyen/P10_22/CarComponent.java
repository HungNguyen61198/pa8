package MinhHung.Nguyen.P10_22;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;

public class CarComponent extends JComponent
{
    private int x;
    private int y;
    private Car car;
    public CarComponent ()
    {
        x = 0;
        y = 0;
        car = new Car(0,0);
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        car.draw(g2);
    }

    public void moveCarBy ( int dx, int dy)
    {
        car.transition(dx,dy);
        repaint();
    }
}