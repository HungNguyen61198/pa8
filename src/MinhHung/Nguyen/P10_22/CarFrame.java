package MinhHung.Nguyen.P10_22;

import javax.swing.*;
import java.awt.event.*;

public class CarFrame extends JFrame {
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 400;
    private final CarComponent scene;

    class TimerListener implements ActionListener{
        public void actionPerformed (ActionEvent event)
        {
            scene.moveCarBy(10,10);
        }
    }

    public CarFrame()
    {
        scene = new CarComponent();
        add(scene);
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
        ActionListener listener = new TimerListener();
        final int DELAY = 1000;
        Timer t = new Timer(DELAY,listener);
        t.start();
    }
}
