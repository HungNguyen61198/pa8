package MinhHung.Nguyen.E3_17;
import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import javax.swing.JComponent;
public class E3_17 {
    public static void main(String[] args)
    {
        JFrame frame = new JFrame();
        frame.setSize(150, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        EllipseComponent component = new EllipseComponent();
        frame.add(component);
        frame.setVisible(true);
    }
}

class EllipseComponent extends JComponent {
    public void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        Graphics2D g1 = (Graphics2D) g;
        Ellipse2D.Double ellipse = new Ellipse2D.Double(0,0,getWidth(),getHeight());
        g2.setColor(Color.PINK);
        g2.fill(ellipse);
        g1.setColor(Color.BLACK);
        g1.draw(ellipse);
    }
}
